﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public Color[] colors, chooseColorsFrom;

    private int randomColor, randomColor2;

    //Color changing functions
    //These functions are called in other scripts

    void Awake()
    {
        randomColor = Random.Range(0, chooseColorsFrom.Length);
        colors[0] = chooseColorsFrom[randomColor];

        do
        {
            randomColor2 = Random.Range(0, chooseColorsFrom.Length);
        } while (randomColor == randomColor2);

        colors[1] = chooseColorsFrom[randomColor2];
    }

    public void SetRandomColor(Renderer obj)
    {
        obj.material.color = colors[Random.Range(0, colors.Length)];
    }

    public void ChangeColor(Renderer from, Renderer to)
    {
        from.material.color = to.material.color;
    }

    public bool CompareColor(Renderer obj1, Renderer obj2)
    {
        return obj1.material.color == obj2.material.color;
    }
}
