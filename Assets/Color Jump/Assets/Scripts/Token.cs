﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    private Transform parentPos;
    private Vector3 tokenPos;

    void Start()
    {
        parentPos = GetComponentInParent<Transform>();      //Inizializes the position of the parent
        transform.localScale = new Vector3(0.4f, 1f, 1f);
    }

    void Update()
    {
        tokenPos = parentPos.position;
        tokenPos.y = 0.2f;
        transform.position = tokenPos;      //Makes the token follow the parent
    }
}
