﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))       //If gameObject collides with an obstacle
        {
            other.GetComponent<Animation>().Play("ObstacleDeathAnim");      //Plays animation
            Destroy(other.gameObject, 2f);      //Destroys obstacle after x seconds
        }
    }
}
